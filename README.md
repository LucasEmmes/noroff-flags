# Flag-recognising bot
Using OpenCV, I have given this robot the power to recognize flags.

If it sees a Ukrainian flag, it will approach it.
If it sees a Russian flag on the other hand, it will flee.